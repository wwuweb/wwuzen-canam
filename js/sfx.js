// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.siteNameTypography = {
		attach: function () {

		// Gets copy from site name (ultimately inside a span: #site-name a span)
		var siteName = $(".site-name").text();

		var typographyAnd = siteName.replace("and", "<span class=\"diminutive-type site-name-and\">and</span>");
		var typographyCollegeOf = typographyAnd.replace("Center for", "<span class=\"diminutive-type site-name-college-of\">Center for</span><br />");

		$('.site-name p span').replaceWith(typographyCollegeOf);
	}
}

})(jQuery, Drupal, this, this.document);


